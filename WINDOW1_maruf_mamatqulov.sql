WITH ranked_customers AS (
  SELECT
    c.customer_id,
    p.payment_date,
    SUM(p.amount) AS total_sales,
    RANK() OVER (PARTITION BY EXTRACT(YEAR FROM p.payment_date) ORDER BY SUM(p.amount) DESC) AS sales_rank
  FROM
    payment p
    INNER JOIN customer c ON p.customer_id = c.customer_id
  WHERE
    EXTRACT(YEAR FROM p.payment_date) IN (1998, 1999, 2001)
  GROUP BY
    c.customer_id,
    p.payment_date
)
SELECT
  c.customer_id,
  c.first_name,
  c.last_name,
  c.email,
  rc.payment_date,
  rc.total_sales,
  rc.sales_rank
FROM
  ranked_customers rc
  INNER JOIN customer c ON rc.customer_id = c.customer_id
WHERE
  rc.sales_rank <= 300
ORDER BY
  rc.payment_date,
  rc.sales_rank;